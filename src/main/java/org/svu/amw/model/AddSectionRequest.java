package org.svu.amw.model;

import lombok.Data;
import org.svu.amw.entity.Section;

@Data
public class AddSectionRequest {
    private String courseId;
    private String instructorId;
    private Section section;
}
