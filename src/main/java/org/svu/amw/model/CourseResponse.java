package org.svu.amw.model;

import lombok.Getter;
import lombok.Setter;
import org.svu.amw.entity.Course;
import org.svu.amw.entity.Section;

import java.util.Set;

@Getter
@Setter
public class CourseResponse {

    public CourseResponse(String id, String title, long hours, Set<SectionResponse> sections) {
        this.id = id;
        this.title = title;
        this.hours = hours;
        this.sections = sections;
    }

    public CourseResponse(Course course, Set<SectionResponse> sections) {
        this.id = course.getId();
        this.title = course.getTitle();
        this.hours = course.getHours();
        this.sections = sections;
    }


    private String id;

    private String title;

    private long hours;

    private Set<SectionResponse> sections;

}
