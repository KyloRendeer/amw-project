package org.svu.amw.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.svu.amw.entity.Enrollment;

import javax.persistence.CascadeType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
public class StudentResponse {
    public StudentResponse(String id, String firstname, String lastname, String registrationYear, Gender gender, String address, String mobile, Set<EnrollmentResponse> enrollments) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.registrationYear = registrationYear;
        this.gender = gender;
        this.address = address;
        this.mobile = mobile;
        this.enrollments = enrollments;
    }

    private String id;

    private String firstname;

    private String lastname;


    private String registrationYear;

    private Gender gender;

    private String address;

    private String mobile;

    private Set<EnrollmentResponse> enrollments;

}
