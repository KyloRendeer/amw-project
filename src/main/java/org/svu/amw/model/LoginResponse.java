package org.svu.amw.model;

import lombok.Data;

@Data
public class LoginResponse {

    public LoginResponse(StudentResponse studentResponse, boolean isAdmin) {
        this.studentResponse = studentResponse;
        this.isAdmin = isAdmin;
    }

    private StudentResponse studentResponse;
    private boolean isAdmin;
}
