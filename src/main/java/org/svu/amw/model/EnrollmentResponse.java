package org.svu.amw.model;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.svu.amw.entity.Course;
import org.svu.amw.entity.Enrollment;
import org.svu.amw.entity.Student;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
public class EnrollmentResponse {
    public EnrollmentResponse(CourseResponse course, int sectionNumber, double grade) {
        this.course = course;
        this.sectionNumber = sectionNumber;
        this.grade = grade;
    }

    private CourseResponse course;

    private int sectionNumber;

    private double grade;

}
