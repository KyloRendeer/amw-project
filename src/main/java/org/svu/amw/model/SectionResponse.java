package org.svu.amw.model;

import lombok.Getter;
import lombok.Setter;
import org.svu.amw.entity.Instructor;
import org.svu.amw.entity.Section;

import java.util.Date;

@Getter
@Setter
public class SectionResponse {
    public SectionResponse(String id, Instructor instructor, int sectionNumber, int roomNumber, Date time, long hours) {
        this.id = id;
        this.instructor = instructor;
        this.sectionNumber = sectionNumber;
        this.roomNumber = roomNumber;
        this.time = time;
        this.hours = hours;
    }
    public SectionResponse(Section section) {
        this.id = section.getId();
        this.instructor = section.getInstructor();
        this.sectionNumber = section.getSectionNumber();
        this.roomNumber = section.getRoomNumber();
        this.time = section.getTime();
        this.hours = section.getHours();
    }

    private String id;

    private Instructor instructor;

    private int sectionNumber;
    private int roomNumber;
    private Date time;
    private long hours;


}
