package org.svu.amw.model;

import lombok.Data;
import org.svu.amw.entity.Enrollment;

@Data
public class EnrollmentRequest {
    private Enrollment enrollment;
    private String courseId;
}
