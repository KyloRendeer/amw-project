package org.svu.amw.model;

import lombok.Data;

@Data
public class LoginRequest {
    private String firstName;
    private String password;
}
