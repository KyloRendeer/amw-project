package org.svu.amw.repository;

import org.springframework.data.repository.CrudRepository;
import org.svu.amw.entity.Course;
import org.svu.amw.entity.Instructor;

public interface InstructorRepository extends CrudRepository<Instructor, String> {
}
