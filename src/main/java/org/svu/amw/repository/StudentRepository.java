package org.svu.amw.repository;

import org.springframework.data.repository.CrudRepository;
import org.svu.amw.entity.Student;

import java.util.Optional;

public interface StudentRepository extends CrudRepository<Student, String> {
    Optional<Student> findStudentByFirstname(String firstName);
}
