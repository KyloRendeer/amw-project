package org.svu.amw.repository;

import org.springframework.data.repository.CrudRepository;
import org.svu.amw.entity.Course;
import org.svu.amw.entity.Student;

public interface CourseRepository extends CrudRepository<Course, String> {
}
