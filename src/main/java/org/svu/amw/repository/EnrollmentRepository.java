package org.svu.amw.repository;

import org.springframework.data.repository.CrudRepository;
import org.svu.amw.entity.Enrollment;
import org.svu.amw.entity.Student;

public interface EnrollmentRepository extends CrudRepository<Enrollment, String> {
}
