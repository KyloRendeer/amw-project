package org.svu.amw.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.svu.amw.entity.Course;
import org.svu.amw.entity.Instructor;
import org.svu.amw.entity.Section;
import org.svu.amw.model.AddSectionRequest;
import org.svu.amw.model.SectionResponse;
import org.svu.amw.repository.CourseRepository;
import org.svu.amw.repository.InstructorRepository;
import org.svu.amw.repository.SectionRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/sections",
        produces = "application/json")
@Validated
public class SectionsController {

    private final SectionRepository sectionRepository;
    private final CourseRepository courseRepository;
    private final InstructorRepository instructorRepository;

    @Value("${amw.admin-id}")
    private String adminId;

    public SectionsController(SectionRepository sectionRepository, CourseRepository courseRepository, InstructorRepository instructorRepository) {
        this.sectionRepository = sectionRepository;
        this.courseRepository = courseRepository;
        this.instructorRepository = instructorRepository;
    }

    @GetMapping
    public ResponseEntity<Object> getSections() {
        List<Section> sections = (List<Section>) sectionRepository.findAll();
        List<SectionResponse> sectionResponses = sections
                .stream()
                .map(SectionResponse::new)
                .collect(Collectors.toList());
        return new ResponseEntity<>(sectionRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{sectionId}")
    public ResponseEntity<Object> getSectionDetails(@PathVariable("sectionId") String sectionId) {
        return new ResponseEntity<>(sectionRepository.findById(sectionId), HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> addSection(@RequestHeader("user-id") String userId, @RequestBody @Valid AddSectionRequest request) {
        ResponseEntity<Object> securityResponse = SecurityController.handleAdmin(userId, adminId);
        if (securityResponse != null)
            return securityResponse;
        Optional<Course> courseOptional = courseRepository.findById(request.getCourseId());
        if (courseOptional.isPresent()) {
            Optional<Instructor> instructorOptional = instructorRepository.findById(request.getInstructorId());
            if (instructorOptional.isPresent()) {
                Section section = request.getSection();
                section.setCourse(courseOptional.get());
                section.setInstructor(instructorOptional.get());
                return new ResponseEntity<>(sectionRepository.save(section), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>("No such instructor", HttpStatus.NOT_FOUND);
            }
        } else
            return new ResponseEntity<>("No such course", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{sectionId}")
    public ResponseEntity<Object> deleteSection(@RequestHeader("user-id") String userId, @PathVariable("sectionId") String sectionId) {
        ResponseEntity<Object> securityResponse = SecurityController.handleAdmin(userId, adminId);
        if (securityResponse != null)
            return securityResponse;
        try {
            courseRepository.deleteById(sectionId);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("", HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value = "/{sectionId}", consumes = "application/json")
    public ResponseEntity<Object> patchSection(@RequestHeader("user-id") String userId, @PathVariable("sectionId") String sectionId, @RequestBody Section patch) {
        ResponseEntity<Object> securityResponse = SecurityController.handleAdmin(userId, adminId);
        if (securityResponse != null)
            return securityResponse;
        Optional<Section> sectionOptional = sectionRepository.findById(sectionId);
        if (sectionOptional.isPresent()) {
            Section section = sectionOptional.get();
            if (patch.getSectionNumber() != 0) {
                section.setSectionNumber(patch.getSectionNumber());
            }
            if (patch.getRoomNumber() != 0) {
                section.setRoomNumber(patch.getRoomNumber());
            }
            if (patch.getTime() != null) {
                section.setTime(patch.getTime());
            }
            if (patch.getHours() != 0L) {
                section.setHours(patch.getHours());
            }
            return new ResponseEntity<>(sectionRepository.save(section), HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>("No such section.", HttpStatus.NOT_FOUND);
    }
}