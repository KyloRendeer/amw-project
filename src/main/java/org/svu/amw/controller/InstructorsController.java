package org.svu.amw.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.svu.amw.entity.Instructor;
import org.svu.amw.repository.InstructorRepository;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(path = "/instructors",
        produces = "application/json")
@Validated
public class InstructorsController {

    private final InstructorRepository instructorRepository;

    @Value("${amw.admin-id}")
    private String adminId;

    public InstructorsController(InstructorRepository instructorRepository) {
        this.instructorRepository = instructorRepository;
    }

    @GetMapping
    public ResponseEntity<Object> getInstructors(@RequestHeader("user-id") String userId) {
        ResponseEntity<Object> securityResponse = SecurityController.handleAdmin(userId, adminId);
        if (securityResponse != null)
            return securityResponse;
        return new ResponseEntity<>(instructorRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{instructorId}")
    public ResponseEntity<Object> getInstructorDetails(@RequestHeader("user-id") String userId, @PathVariable("instructorId") String instructorId) {
        ResponseEntity<Object> securityResponse = SecurityController.handleAdmin(userId, adminId);
        if (securityResponse != null)
            return securityResponse;
        return new ResponseEntity<>(instructorRepository.findById(instructorId), HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> addInstructor(@RequestHeader("user-id") String userId, @RequestBody @Valid Instructor instructor) {
        ResponseEntity<Object> securityResponse = SecurityController.handleAdmin(userId, adminId);
        if (securityResponse != null)
            return securityResponse;
        return new ResponseEntity<>(instructorRepository.save(instructor), HttpStatus.CREATED);
    }

    @DeleteMapping("/{instructorId}")
    public ResponseEntity<Object> deleteInstructor(@RequestHeader("user-id") String userId, @PathVariable("instructorId") String instructorId) {
        ResponseEntity<Object> securityResponse = SecurityController.handleAdmin(userId, adminId);
        if (securityResponse != null)
            return securityResponse;
        try {
            instructorRepository.deleteById(instructorId);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("", HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value = "/{instructorId}", consumes = "application/json")
    public ResponseEntity<Object> patchInstructor(@RequestHeader("user-id") String userId, @PathVariable("instructorId") String instructorId, @RequestBody Instructor patch) {
        ResponseEntity<Object> securityResponse = SecurityController.handleAdmin(userId, adminId);
        if (securityResponse != null)
            return securityResponse;
        Optional<Instructor> instructorOptional = instructorRepository.findById(instructorId);
        if (instructorOptional.isPresent()) {
            Instructor instructor = instructorOptional.get();
            if (patch.getFirstname() != null) {
                instructor.setFirstname(patch.getFirstname());
            }
            if (patch.getLastname() != null) {
                instructor.setLastname(patch.getLastname());
            }
            if (patch.getGender() != null) {
                instructor.setGender(patch.getGender());
            }
            if (patch.getAddress() != null) {
                instructor.setAddress(patch.getAddress());
            }
            if (patch.getMobile() != null) {
                instructor.setMobile(patch.getMobile());
            }

            return new ResponseEntity<>(instructorRepository.save(instructor), HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>("No such instructor.", HttpStatus.NOT_FOUND);
    }
}