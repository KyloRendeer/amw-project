package org.svu.amw.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class SecurityController {
    public static ResponseEntity<Object> handleAdmin(String userId, String adminId) {
        if (userId == null) {
            return new ResponseEntity<>("user-id header is missing", HttpStatus.BAD_REQUEST);
        } else if (userId.isEmpty()) {
            return new ResponseEntity<>("user-id header value is invalid", HttpStatus.BAD_REQUEST);
        } else if (!userId.equals(adminId)) {
            return new ResponseEntity<>("User is not an admin", HttpStatus.FORBIDDEN);
        }
        return null;
    }
}
