package org.svu.amw.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.svu.amw.entity.Course;
import org.svu.amw.model.CourseResponse;
import org.svu.amw.model.SectionResponse;
import org.svu.amw.repository.CourseRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/courses",
        produces = "application/json")
@Validated
public class CoursesController {

    private final CourseRepository courseRepository;

    @Value("${amw.admin-id}")
    private String adminId;

    public CoursesController(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @GetMapping
    public ResponseEntity<Object> getCourses() {
        List<Course> courses = (List<Course>) courseRepository.findAll();
        List<CourseResponse> courseResponses = courses.stream()
                .map(course -> new CourseResponse(course.getId(), course.getTitle(), course.getHours(), course.getSections().stream()
                        .map(section -> new SectionResponse(section.getId(), section.getInstructor(), section.getSectionNumber(), section.getRoomNumber(), section.getTime(), section.getHours())).collect(Collectors.toSet())))
                .collect(Collectors.toList());
        return new ResponseEntity<>(courseResponses, HttpStatus.OK);
    }

    @GetMapping("/{courseId}")
    public ResponseEntity<Object> getCourseDetails(@PathVariable("courseId") String courseId) {
        return new ResponseEntity<>(courseRepository.findById(courseId), HttpStatus.OK);
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> addCourse(@RequestHeader("user-id") String userId, @RequestBody @Valid Course course) {
        ResponseEntity<Object> securityResponse = SecurityController.handleAdmin(userId, adminId);
        if (securityResponse != null)
            return securityResponse;
        return new ResponseEntity<>(courseRepository.save(course), HttpStatus.CREATED);
    }

    @DeleteMapping("/{courseId}")
    public ResponseEntity<Object> deleteCourse(@RequestHeader("user-id") String userId, @PathVariable("courseId") String courseId) {
        ResponseEntity<Object> securityResponse = SecurityController.handleAdmin(userId, adminId);
        if (securityResponse != null)
            return securityResponse;
        try {
            courseRepository.deleteById(courseId);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("", HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value = "/{courseId}", consumes = "application/json")
    public ResponseEntity<Object> patchCourse(@RequestHeader("user-id") String userId, @PathVariable("courseId") String courseId, @RequestBody Course patch) {
        ResponseEntity<Object> securityResponse = SecurityController.handleAdmin(userId, adminId);
        if (securityResponse != null)
            return securityResponse;
        Optional<Course> courseOptional = courseRepository.findById(courseId);
        if (courseOptional.isPresent()) {
            Course course = courseOptional.get();
            if (patch.getTitle() != null) {
                course.setTitle(patch.getTitle());
            }
            if (patch.getHours() != 0L) {
                course.setHours(patch.getHours());
            }

            return new ResponseEntity<>(courseRepository.save(course), HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>("No such course.", HttpStatus.NOT_FOUND);
    }
}