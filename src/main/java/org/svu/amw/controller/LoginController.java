package org.svu.amw.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.svu.amw.BCrypt;
import org.svu.amw.entity.Student;
import org.svu.amw.model.*;
import org.svu.amw.repository.StudentRepository;

import javax.validation.Valid;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/login",
        produces = "application/json")
@Validated
public class LoginController {
    private final StudentRepository studentRepository;

    @Value("${amw.admin-id}")
    private String adminId;

    public LoginController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> enroll(@RequestBody @Valid LoginRequest request) {
        Optional<Student> studentOptional = studentRepository.findStudentByFirstname(request.getFirstName());
        if (studentOptional.isPresent()) {
            Student student = studentOptional.get();
            if (BCrypt.checkpw(request.getPassword(), student.getPassword())) {
                LoginResponse loginResponse = new LoginResponse(
                        new StudentResponse(
                                student.getId(),
                                student.getFirstname(),
                                student.getLastname(),
                                student.getRegistrationYear(),
                                student.getGender(),
                                student.getAddress(),
                                student.getMobile(),
                                student.getEnrollments()
                                        .stream()
                                        .map(enrollment -> new EnrollmentResponse(new CourseResponse(enrollment.getCourse(), enrollment.getCourse().getSections().stream().map(SectionResponse::new).collect(Collectors.toSet())), enrollment.getSectionNumber(), enrollment.getGrade()))
                                        .collect(Collectors.toSet())), student.getId().equals(adminId));
                return new ResponseEntity<>(loginResponse, HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Incorrect credentials", HttpStatus.NOT_FOUND);
            }
        } else
            return new ResponseEntity<>("Incorrect credentials", HttpStatus.NOT_FOUND);
    }
}