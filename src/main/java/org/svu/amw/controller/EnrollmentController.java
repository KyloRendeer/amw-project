package org.svu.amw.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.svu.amw.entity.Course;
import org.svu.amw.entity.Enrollment;
import org.svu.amw.entity.Student;
import org.svu.amw.model.EnrollmentRequest;
import org.svu.amw.repository.CourseRepository;
import org.svu.amw.repository.EnrollmentRepository;
import org.svu.amw.repository.StudentRepository;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(path = "/enrollments",
        produces = "application/json")
@Validated
public class EnrollmentController {

    private final EnrollmentRepository enrollmentRepository;
    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;

    @Value("${amw.admin-id}")
    private String adminId;

    public EnrollmentController(EnrollmentRepository enrollmentRepository, CourseRepository courseRepository, StudentRepository studentRepository) {
        this.enrollmentRepository = enrollmentRepository;
        this.courseRepository = courseRepository;
        this.studentRepository = studentRepository;
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> enroll(@RequestHeader("user-id") String userId, @RequestBody @Valid EnrollmentRequest request) {
        Optional<Course> courseOptional = courseRepository.findById(request.getCourseId());
        if (courseOptional.isPresent()) {
            Optional<Student> studentOptional = studentRepository.findById(userId);
            if (studentOptional.isPresent()) {
                Enrollment enrollment = request.getEnrollment();
                enrollment.setCourse(courseOptional.get());
                enrollment.setStudent(studentOptional.get());
                return new ResponseEntity<>(enrollmentRepository.save(enrollment), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>("No such student", HttpStatus.NOT_FOUND);
            }
        } else
            return new ResponseEntity<>("No such course", HttpStatus.NOT_FOUND);
    }
}