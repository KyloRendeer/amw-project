package org.svu.amw.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.svu.amw.BCrypt;
import org.svu.amw.entity.Student;
import org.svu.amw.model.CourseResponse;
import org.svu.amw.model.EnrollmentResponse;
import org.svu.amw.model.SectionResponse;
import org.svu.amw.model.StudentResponse;
import org.svu.amw.repository.StudentRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/students",
        produces = "application/json")
@Validated
public class StudentController {

    private final StudentRepository studentRepository;

    @Value("${amw.admin-id}")
    private String adminId;

    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @GetMapping
    public ResponseEntity<Object> getStudents(@RequestHeader("user-id") String userId) {
        ResponseEntity<Object> securityResponse = SecurityController.handleAdmin(userId, adminId);
        if (securityResponse != null)
            return securityResponse;
        List<Student> students = (List<Student>) studentRepository.findAll();
        students.removeIf(student -> student.getId().equals(adminId));
        List<StudentResponse> studentResponses = students
                .stream()
                .map(student ->
                        new StudentResponse(student.getId(),
                                student.getFirstname(),
                                student.getLastname(),
                                student.getRegistrationYear(),
                                student.getGender(),
                                student.getAddress(),
                                student.getMobile(),
                                student.getEnrollments()
                                        .stream()
                                        .map(enrollment -> new EnrollmentResponse(new CourseResponse(enrollment.getCourse(), enrollment.getCourse().getSections().stream().map(SectionResponse::new).collect(Collectors.toSet())), enrollment.getSectionNumber(), enrollment.getGrade()))
                                        .collect(Collectors.toSet())
                        )
                ).collect(Collectors.toList());
        return new ResponseEntity<>(studentResponses, HttpStatus.OK);
    }

    @GetMapping("/{studentId}")
    public ResponseEntity<Object> getStudentDetails(@RequestHeader("user-id") String userId, @PathVariable("studentId") String studentId) {
        if (userId.equals(studentId) || userId.equals(adminId)) {
            Optional<Student> studentOptional = studentRepository.findById(studentId);
            return studentOptional
                    .<ResponseEntity<Object>>map(student -> new ResponseEntity<>(student, HttpStatus.OK))
                    .orElseGet(() -> new ResponseEntity<>("No such student.", HttpStatus.NOT_FOUND));
        }
        return SecurityController.handleAdmin(userId, adminId);
    }

    @PostMapping(consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Object> addStudent(@RequestHeader("user-id") String userId, @RequestBody @Valid Student student) {
        ResponseEntity<Object> securityResponse = SecurityController.handleAdmin(userId, adminId);
        if (securityResponse != null)
            return securityResponse;
        student.setPassword(BCrypt.hashpw(student.getPassword(), BCrypt.gensalt()));
        return new ResponseEntity<>(studentRepository.save(student), HttpStatus.CREATED);
    }

    @DeleteMapping("/{studentId}")
    public ResponseEntity<Object> deleteStudent(@RequestHeader("user-id") String userId, @PathVariable("studentId") String studentId) {
        ResponseEntity<Object> securityResponse = SecurityController.handleAdmin(userId, adminId);
        if (securityResponse != null)
            return securityResponse;
        try {
            studentRepository.deleteById(studentId);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("", HttpStatus.NO_CONTENT);
    }

    @PatchMapping(value = "/{studentId}", consumes = "application/json")
    public ResponseEntity<Object> patchStudent(@RequestHeader("user-id") String userId, @PathVariable("studentId") String studentId, @RequestBody Student patch) {
        if (userId.equals(studentId) || userId.equals(adminId)) {
            Optional<Student> studentOptional = studentRepository.findById(studentId);
            if (studentOptional.isPresent()) {
                Student student = studentOptional.get();
                if (patch.getFirstname() != null) {
                    student.setFirstname(patch.getFirstname());
                }
                if (patch.getLastname() != null) {
                    student.setLastname(patch.getLastname());
                }
                if (patch.getPassword() != null) {
                    student.setPassword(BCrypt.hashpw(patch.getPassword(), BCrypt.gensalt()));
                }
                if (patch.getRegistrationYear() != null) {
                    student.setRegistrationYear(patch.getRegistrationYear());
                }
                if (patch.getGender() != null) {
                    student.setGender(patch.getGender());
                }
                if (patch.getAddress() != null) {
                    student.setAddress(patch.getAddress());
                }
                if (patch.getMobile() != null) {
                    student.setMobile(patch.getMobile());
                }

                return new ResponseEntity<>(studentRepository.save(student), HttpStatus.ACCEPTED);
            }
            return new ResponseEntity<>("No such student.", HttpStatus.NOT_FOUND);
        }
        return SecurityController.handleAdmin(userId, adminId);
    }
}